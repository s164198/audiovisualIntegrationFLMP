import torch
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
# import pandas as pd

#pip install array-to-latex
import array_to_latex as a2l

# EARLY MLE model
torch.manual_seed(5)

mode = 'simulated-flmp'

mu_A = torch.arange(1, 5+1)
mu_B = torch.arange(1, 5+1)

# Read the data. We have 5 subjects.
tables = []
for i in range(5):
    tables.append(np.genfromtxt('DataSub' + str(i+1) + '.txt'))

data = torch.as_tensor(tables)

# Parameters
c_A = torch.nn.Parameter(torch.randn((35, 5, 1,))+3)
c_B = torch.nn.Parameter(torch.randn((35, 5, 1))+3)
log_sigma_A = torch.nn.Parameter(3+torch.randn((35, 5, 1,)))
log_sigma_B = torch.nn.Parameter(3+torch.randn((35, 5, 1,)))
# -.5 to .5
mask = torch.eye(35).reshape((35, 1, 7, 5))

# optimizer = torch.optim.SGD([c_A, c_B, log_sigma_A, log_sigma_B], lr=0.002, momentum=0.2, dampening=0.2)
optimizer = torch.optim.Adam([c_A, c_B, log_sigma_A, log_sigma_B], lr=0.5)
loglikes = []
final_loss = None
for i in range(150):
    optimizer.zero_grad()

    mu_hat_A = mu_A - c_A
    mu_hat_B = mu_B - c_B
    dist_A = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_A))
    dist_B = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_B))

    mu_hat_AB = ((dist_B.scale ** 2 * mu_hat_A)[:, :, np.newaxis, :] + (dist_A.scale ** 2 * mu_hat_B)[:, :, :, np.newaxis]) / (
                dist_A.scale[:, :, np.newaxis, :] ** 2 + dist_B.scale[:, :, :, np.newaxis] ** 2)

    dist_AB = torch.distributions.Normal(loc=0,
                                         scale=dist_A.scale[:, :, np.newaxis, :] * dist_B.scale[:, :, :, np.newaxis] / torch.sqrt(dist_A.scale[:, :, np.newaxis, :] ** 2 + dist_B.scale[:, :, :, np.newaxis] ** 2))
    #dist_AB = torch.distributions.Normal(loc=0,
    #                                     scale=dist_A.scale[:, :, np.newaxis, :] * dist_B.scale[:, :, :,
    #                                                                            np.newaxis] / torch.sqrt(
    #                                         dist_A.scale[:, :, np.newaxis, :] ** 2 + dist_B.scale[:, :, :, np.newaxis] ** 2))

    probabilities = torch.cat([dist_A.cdf(mu_hat_A)[..., np.newaxis, :],  # Audio only
                               dist_B.cdf(mu_hat_B)[..., np.newaxis, :],  # Visual only
                               dist_AB.cdf(mu_hat_AB)], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    negative_log_probability_of_data = -distribution.log_prob(data)
    (negative_log_probability_of_data * (1 - mask)).sum().backward()  # We assume independent trails, so we can sum it all.
    optimizer.step()

    negative_log_probability_of_data = negative_log_probability_of_data.detach()
    nll_loss = (negative_log_probability_of_data * mask).sum().item()
    loglikes.append(nll_loss)
    final_loss = np.array((negative_log_probability_of_data * mask).sum([0, 2, 3]))

offset = 20
plt.plot(range(offset, len(loglikes)), loglikes[offset:])
plt.show()
matr = np.around(np.array((negative_log_probability_of_data * (1-mask)).sum([2, 3]).permute([1, 0]).reshape(5, 7, 5)), 2)
for i in range(5):
    ltxmatr = a2l.to_ltx(matr[i], frmt = '{:6.2f}', arraytype = 'array')
    print(ltxmatr)

#print('Final negative log likelihood training error for each fold for each subject', np.around(np.array((negative_log_probability_of_data * (1-mask)).sum([2, 3]).permute([1, 0]).reshape(5, 7, 5)), 2))
print('Final negative log likelihood test error', final_loss)

print('done')
if False:
    print('A:')
    print(torch.stack([c_A, torch.exp(log_sigma_A)], -1))
    print('B:')
    print(torch.stack([c_B, torch.exp(log_sigma_B)], -1))

if True:
    left_out_predicted_probabilities = probabilities.permute([1, 0, 2, 3]).detach().masked_select(mask.permute([1, 0, 2, 3]).bool()).reshape(probabilities.shape[-3:]).numpy()
    for subject_idx in range(len(data)):
        plt.plot((data[subject_idx] / 24).flatten(), left_out_predicted_probabilities[subject_idx].flatten(), '.')
        plt.title('Test: Subject ' + str(subject_idx+1))
        plt.xlabel('Response proportion')
        plt.ylabel('MLE response probabilities')
        plt.show()

# The fit is worse than for the FLMP, but the FLMP also used 50 parameters, while this one only uses 20.
sigma_AB = dist_AB.scale
c_AB = ((dist_B.scale ** 2 * c_A)[:, :, np.newaxis, :] + (dist_A.scale ** 2 * c_B)[:, :, :, np.newaxis]) / (
                dist_A.scale[:, :, np.newaxis, :] ** 2 + dist_B.scale[:, :, :, np.newaxis] ** 2)

if False:
    for i in range(len(data)):
        x = np.linspace(0, 6, 100)
        y_AB = scipy.stats.norm(loc=c_AB[i].item(), scale=sigma_AB[0].item()).pdf(x)
        y_A = scipy.stats.norm(loc=c_A[i].item(), scale=np.exp(log_sigma_A[0].item())).pdf(x)
        y_B = scipy.stats.norm(loc=c_B[i].item(), scale=np.exp(log_sigma_B[0].item())).pdf(x)
        plt.plot(x, y_A, '--y')
        plt.plot(x, y_B, '--r')
        plt.plot(x, y_AB, '-r')
        plt.title('Test: Subject ' + str(i+1))
        plt.ylabel('Probability density')
        plt.xlabel('Internal representation')
        plt.legend(['Audio', 'Visual', 'AV'])
        plt.xticks([1, 2, 3, 4, 5])
        plt.grid(True, axis='x')
        # plt.vlines([3], 0, np.max(y_AB))
        plt.show()


