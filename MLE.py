import torch
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
# import pandas as pd

# EARLY MLE model
torch.manual_seed(5)

mode = 'normal'

mu_A = torch.arange(1, 5+1)
mu_B = torch.arange(1, 5+1)

if mode == 'normal':
    # Read the data. We have 5 subjects.
    tables = []
    for i in range(5):
        tables.append(np.genfromtxt('DataSub' + str(i+1) + '.txt'))

    data = torch.as_tensor(tables)
elif mode == 'simulated-MLE':
    c_A, c_B, log_sigma_A, log_sigma_B = torch.load('MLE.pt')
    mu_hat_A = mu_A - c_A
    mu_hat_B = mu_B - c_B
    dist_A = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_A))
    dist_B = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_B))

    mu_hat_AB = ((dist_B.scale ** 2 * mu_hat_A)[:, np.newaxis, :] + (dist_A.scale ** 2 * mu_hat_B)[:, :,
                                                                    np.newaxis]) / (
                        dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2)

    dist_AB = torch.distributions.Normal(loc=0,
                                         scale=dist_A.scale[:, np.newaxis, :] * dist_B.scale[:, :, np.newaxis] / torch.sqrt(
                                               dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2))
    probabilities = torch.cat([dist_A.cdf(mu_hat_A)[..., np.newaxis, :],  # Audio only
                               dist_B.cdf(mu_hat_B)[..., np.newaxis, :],  # Visual only
                               dist_AB.cdf(mu_hat_AB)], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)
    data = distribution.sample()

    print('Simulated model based on learned subject probabilities.')
    print('Parameters')
    print('c_A:', c_A)
    print('c_V:', c_B)
    print('sigma_A:', torch.exp(log_sigma_A))
    print('sigma_V:', torch.exp(log_sigma_B))
    print('Simulated data:', data)
elif mode == 'simulated-flmp':
    parameter_audio, parameter_visual = torch.load('flmp.pt')
    p_audio = torch.sigmoid(parameter_audio)  # = torch.exp(parameter_audio) / (torch.exp(parameter_audio) + 1)
    p_visual = torch.sigmoid(parameter_visual)
    print('Simulated model based on learned subject probabilities.')
    print('Parameters')
    print('p_audio:', p_audio)
    print('p_visual:', p_visual)
    probabilities = torch.cat([p_audio,  # Audio only
                               p_visual.permute([0, 2, 1]),  # Visual only
                               p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    data = distribution.sample()
    print('Simulated data:', data)

# Parameters
c_A = torch.nn.Parameter(torch.randn((5, 1,))+3)
c_B = torch.nn.Parameter(torch.randn((5, 1))+3)
log_sigma_A = torch.nn.Parameter(3+torch.randn((5, 1,)))
log_sigma_B = torch.nn.Parameter(3+torch.randn((5, 1,)))
# -.5 to .5

# optimizer = torch.optim.SGD([c_A, c_B, log_sigma_A, log_sigma_B], lr=0.002, momentum=0.2, dampening=0.2)
optimizer = torch.optim.Adam([c_A, c_B, log_sigma_A, log_sigma_B], lr=0.5)
loglikes = []
final_loss = None
for i in range(150):
    optimizer.zero_grad()

    mu_hat_A = mu_A - c_A
    mu_hat_B = mu_B - c_B
    dist_A = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_A))
    dist_B = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_B))

    mu_hat_AB = ((dist_B.scale ** 2 * mu_hat_A)[:, np.newaxis, :] + (dist_A.scale ** 2 * mu_hat_B)[:, :, np.newaxis]) / (
                dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2)

    dist_AB = torch.distributions.Normal(loc=0,
                                         scale=dist_A.scale[:, np.newaxis, :] * dist_B.scale[:, :, np.newaxis] / torch.sqrt(dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2))
    #dist_AB = torch.distributions.Normal(loc=0,
    #                                     scale=dist_A.scale[:, np.newaxis, :] * dist_B.scale[:, :,
    #                                                                            np.newaxis] / torch.sqrt(
    #                                         dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2))

    probabilities = torch.cat([dist_A.cdf(mu_hat_A)[..., np.newaxis, :],  # Audio only
                               dist_B.cdf(mu_hat_B)[..., np.newaxis, :],  # Visual only
                               dist_AB.cdf(mu_hat_AB)], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    negative_log_probability_of_data = -distribution.log_prob(data)
    negative_log_probability_of_data.sum().backward()  # We assume independent trails, so we can sum it all.
    optimizer.step()

    negative_log_probability_of_data = negative_log_probability_of_data.detach()
    nll_loss = negative_log_probability_of_data.sum().item()
    loglikes.append(nll_loss)
    final_loss = np.array(negative_log_probability_of_data.sum([1, 2]))

    if i % 5 == 0 and False:
        print(f'Step {i}.\n'
              f'Negative log probability of data: {nll_loss} -', np.array(negative_log_probability_of_data.sum([1, 2])), '\n',
          np.around(np.array(probabilities.detach()) * 24, 1))

if mode == 'normal':
    torch.save([c_A, c_B, log_sigma_A, log_sigma_B], 'MLE.pt')

offset = 20
plt.plot(range(offset, len(loglikes)), loglikes[offset:])
plt.show()
print('Final negative log likelihood', final_loss)
print('done')
print('A:')
print(torch.stack([c_A, torch.exp(log_sigma_A)], -1))
print('B:')
print(torch.stack([c_B, torch.exp(log_sigma_B)], -1))

if True:
    probabilities = probabilities.detach().numpy()
    for subject_idx in range(len(data)):
        plt.plot((data[subject_idx] / 24).flatten(), probabilities[subject_idx].flatten(), '.')
        plt.title('Subject ' + str(subject_idx+1))
        plt.xlabel('Response proportion')
        plt.ylabel('MLE response probabilities')
        plt.show()

# The fit is worse than for the FLMP, but the FLMP also used 50 parameters, while this one only uses 20.
sigma_AB = dist_AB.scale
c_AB = ((dist_B.scale ** 2 * c_A)[:, np.newaxis, :] + (dist_A.scale ** 2 * c_B)[:, :, np.newaxis]) / (
                dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2)

if True:
    for i in range(len(data)):
        x = np.linspace(0, 6, 100)
        y_AB = scipy.stats.norm(loc=c_AB[i].item(), scale=sigma_AB[0].item()).pdf(x)
        y_A = scipy.stats.norm(loc=c_A[i].item(), scale=np.exp(log_sigma_A[0].item())).pdf(x)
        y_B = scipy.stats.norm(loc=c_B[i].item(), scale=np.exp(log_sigma_B[0].item())).pdf(x)
        plt.plot(x, y_A, '--y')
        plt.plot(x, y_B, '--r')
        plt.plot(x, y_AB, '-r')
        plt.title('Subject ' + str(i+1))
        plt.ylabel('Probability density')
        plt.xlabel('Internal representation')
        plt.legend(['Audio', 'Visual', 'AV'])
        plt.xticks([1, 2, 3, 4, 5])
        plt.grid(True, axis='x')
        # plt.vlines([3], 0, np.max(y_AB))
        plt.show()


