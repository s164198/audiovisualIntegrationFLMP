import torch
import numpy as np
import matplotlib.pyplot as plt
# import pandas as pd

torch.manual_seed(5)

mode = 'normal'

mu_A = torch.arange(1, 5+1)
mu_B = torch.arange(1, 5+1)

if mode == 'normal':
    # Read the data. We have 5 subjects.
    tables = []
    for i in range(5):
        tables.append(np.genfromtxt('DataSub' + str(i+1) + '.txt'))

    # Columns contain the response counts for the stimulus being close to 'b' (column 1) or closer to 'd' (column 5)
    # The first row is auditory stimulus response
    # The second row is visual stimulus response
    # The last rows are audiovisual stimulus responses,
    #  where row 3 has the visual component very similar to 'b'
    #    and row 7 has the visual component very similar to 'd'.

    data = torch.as_tensor(tables)
elif mode == 'simulated-flmp':
    parameter_audio, parameter_visual = torch.load('flmp.pt')
    p_audio = torch.sigmoid(parameter_audio)  # = torch.exp(parameter_audio) / (torch.exp(parameter_audio) + 1)
    p_visual = torch.sigmoid(parameter_visual)
    print('Simulated model based on learned subject probabilities.')
    print('Parameters')
    print('p_audio:', p_audio)
    print('p_visual:', p_visual)
    probabilities = torch.cat([p_audio,  # Audio only
                               p_visual.permute([0, 2, 1]),  # Visual only
                               p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    data = distribution.sample()
    print('Simulated data:', data)
elif mode == 'simulated-MLE':
    c_A, c_B, log_sigma_A, log_sigma_B = torch.load('MLE.pt')
    mu_hat_A = mu_A - c_A
    mu_hat_B = mu_B - c_B
    dist_A = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_A))
    dist_B = torch.distributions.Normal(loc=0, scale=torch.exp(log_sigma_B))

    mu_hat_AB = ((dist_B.scale ** 2 * mu_hat_A)[:, np.newaxis, :] + (dist_A.scale ** 2 * mu_hat_B)[:, :,
                                                                    np.newaxis]) / (
                        dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2)

    dist_AB = torch.distributions.Normal(loc=0,
                                         scale=dist_A.scale[:, np.newaxis, :] * dist_B.scale[:, :, np.newaxis] / torch.sqrt(
                                               dist_A.scale[:, np.newaxis, :] ** 2 + dist_B.scale[:, :, np.newaxis] ** 2))
    probabilities = torch.cat([dist_A.cdf(mu_hat_A)[..., np.newaxis, :],  # Audio only
                               dist_B.cdf(mu_hat_B)[..., np.newaxis, :],  # Visual only
                               dist_AB.cdf(mu_hat_AB)], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)
    data = distribution.sample()

    print('Simulated model based on learned subject probabilities.')
    print('Parameters')
    print('c_A:', c_A)
    print('c_V:', c_B)
    print('sigma_A:', torch.exp(log_sigma_A))
    print('sigma_V:', torch.exp(log_sigma_B))
    print('Simulated data:', data)
elif mode == 'simulated-random-flmp':
    p_audio = torch.rand((5, 1, 5))
    p_visual = torch.rand((5, 5, 1))
    print('Simulated models with based on random free parameters.')
    print('Parameters')
    print('p_audio:', p_audio)
    print('p_visual:', p_visual)
    probabilities = torch.cat([p_audio,  # Audio only
                               p_visual.permute([0, 2, 1]),  # Visual only
                               p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    data = distribution.sample()
    print('Simulated data:', data)


# The responses vary from 0 to 24, and is the number of times subjects rate the stimuli to be perceived as 'd'.
# probabilities = data / 24

# P('d' | auditory similar to 'b')

parameter_audio = torch.nn.Parameter(torch.randn((5, 1, data.shape[-1])))
parameter_visual = torch.nn.Parameter(torch.randn((5, 5, 1)))

# withaudio = torch.as_tensor([True, False, True, True, True, True, True])
# withimage = torch.as_tensor([False, True, True, True, True, True, True])
# What we want to satisfy:
# probabilities[:, withaudio & ~withimage, :] == p_audio
# probabilities[:, ~withaudio & withimage, :] == p_visual.T
# probabilities[:, withaudio & withimage, :] == p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))

optimizer = torch.optim.SGD([parameter_audio, parameter_visual], lr=0.01)
# optimizer = torch.optim.Adam([parameter_audio, parameter_visual], lr=1e-1)
loglikes = []
final_loss = None
for i in range(500):
    optimizer.zero_grad()

    p_audio = torch.sigmoid(parameter_audio)  # = torch.exp(parameter_audio) / (torch.exp(parameter_audio) + 1)
    p_visual = torch.sigmoid(parameter_visual)
    probabilities = torch.cat([p_audio,  # Audio only
                               p_visual.permute([0, 2, 1]),  # Visual only
                               p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    negative_log_probability_of_data = -distribution.log_prob(data)
    negative_log_probability_of_data.sum().backward()  # We assume independent trails, so we can sum it all.
    optimizer.step()

    negative_log_probability_of_data = negative_log_probability_of_data.detach()
    nll_loss = negative_log_probability_of_data.sum().item()
    loglikes.append(nll_loss)
    final_loss = np.array(negative_log_probability_of_data.sum([1, 2]))
    if i % 5 == 0 and False:
        print(f'Step {i}.\n'
              f'Negative log probability of data: {nll_loss} -', np.array(negative_log_probability_of_data.sum([1, 2])), '\n',
          np.around(np.array(probabilities.detach()) * 24, 1))

if mode == 'normal':
    torch.save([parameter_audio, parameter_visual], 'flmp.pt')

offset = 100
plt.plot(range(offset, len(loglikes)), loglikes[offset:])
plt.show()
print('Final negative log likelihood', final_loss)
print('done')

if True:
    probabilities = probabilities.detach().numpy()
    for subject_idx in range(len(data)):
        plt.plot((data[subject_idx] / 24).flatten(), probabilities[subject_idx].flatten(), '.')
        plt.title('Subject ' + str(subject_idx+1))
        plt.xlabel('Response proportion')
        plt.ylabel('MLE response probabilities')
        plt.show()
