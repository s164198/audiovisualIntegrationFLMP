import torch
import numpy as np
import matplotlib.pyplot as plt
# import pandas as pd

#pip install array-to-latex
import array_to_latex as a2l

torch.manual_seed(5)

# Read the data. We have 5 subjects.
tables = []
for i in range(5):
    tables.append(np.genfromtxt('DataSub' + str(i+1) + '.txt'))

data = torch.as_tensor(tables)

# TODO: Initialize all models at same point or different points?
parameter_audio = torch.nn.Parameter(torch.randn((7*5, 5, 1, data.shape[-1])))
parameter_visual = torch.nn.Parameter(torch.randn((7*5, 5, 5, 1)))

mask = torch.eye(35).reshape((35, 1, 7, 5))
optimizer = torch.optim.SGD([parameter_audio, parameter_visual], lr=0.01)
# optimizer = torch.optim.Adam([parameter_audio, parameter_visual], lr=1e-1)
loglikes = []
final_loss = None

best_parameters = parameter_audio.detach().clone(), parameter_visual.detach().clone()
best_loss = [float('inf')] * 5
for i in range(150):  # 150 or maybe 3000 to ensure convergence (to an overfitted sub-optimum solution)
    optimizer.zero_grad()

    p_audio = torch.sigmoid(parameter_audio)  # = torch.exp(parameter_audio) / (torch.exp(parameter_audio) + 1)
    p_visual = torch.sigmoid(parameter_visual)
    probabilities = torch.cat([p_audio,  # Audio only
                               p_visual.permute([0, 1, 3, 2]),  # Visual only
                               p_audio * p_visual / (p_audio * p_visual + (1 - p_audio) * (1 - p_visual))], -2)  # Both
    distribution = torch.distributions.binomial.Binomial(total_count=24, probs=probabilities)

    negative_log_probability_of_data = -distribution.log_prob(data)
    (negative_log_probability_of_data * (1-mask)).sum().backward()  # We assume independent trails, so we can sum it all.
    optimizer.step()

    negative_log_probability_of_data = negative_log_probability_of_data.detach()
    nll_loss = (negative_log_probability_of_data * mask).sum().item()
    loglikes.append(nll_loss)
    final_loss = np.array((negative_log_probability_of_data * mask).sum([0, 2, 3]))

offset = 100
plt.plot(range(offset, len(loglikes)), loglikes[offset:])
plt.show()
matr = np.around(np.array((negative_log_probability_of_data * (1-mask)).sum([2, 3]).permute([1, 0]).reshape(5, 7, 5)), 2)
for i in range(5):
    ltxmatr = a2l.to_ltx(matr[i], frmt = '{:6.2f}', arraytype = 'array')
    print(ltxmatr)
#print('Final negative log likelihood training error for each fold for each subject', matr)
print('Final negative log likelihood test error', final_loss)
# print('Final negative log likelihood test error for each subject for each model', np.around(np.array((negative_log_probability_of_data * mask).sum([2, 3]).permute([1, 0]).reshape(5, 7, 5)), 2))
print('done')

if False:
    probabilities = probabilities.detach().numpy()
    for subject_idx in range(len(data)):
        plt.plot((data[subject_idx] / 24).flatten(), probabilities[subject_idx].flatten(), '.')
        plt.title('Subject ' + str(subject_idx+1))
        plt.xlabel('Response proportion')
        plt.ylabel('MLE response probabilities')
        plt.show()
